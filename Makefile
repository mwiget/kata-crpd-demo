# Copyright (c) 2020, Juniper Networks, Inc.
# All rights reserved.

all: up

up:
	docker-compose up -d
	./validate.sh

down:
	docker-compose down

ps:
	docker-compose ps
