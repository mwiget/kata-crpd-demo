# kata-crpd-demo

This is a quick test with 4 cRPD (20.1R1.11) connected in series and launched using kata-container runtime.
cRPD requires privilege mode in order to inject learned routes into the containers network namespace. 
Using [kata containers](https://katacontainers.io), these routes will be learned within the kata VM instead 
of the host directly. This little demo setup successfully proofs this.

Instructions on how to install kata container as an alternative runtime to runc, check out the following
blog post: [Container isolation with Kata and gVisor in Docker](https://marcelwiget.blog/2020/03/24/container-isolation-with-kata-and-gvisor-in-docker/)

```mermaid
graph LR
r1 ---|r1r2| r2 ---|r2r3| r3 ---|r3r4| r4
```

To launch the demo, use `make up` or `docker-compose up -d` followed by `./validate.sh` to verify connectivity and routing tables:

```
$ make up
docker-compose up -d
./validate.sh

kernel version: Linux rigi 4.15.18-custom #2 SMP Thu Feb 13 22:49:55 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
executed from /home/mwiget/kata-crpd-demo on rigi

waiting since 1 secs for 10.34.34.0/24 route to show up ...
waiting since 11 secs for 10.34.34.0/24 route to show up ...
waiting since 21 secs for 10.34.34.0/24 route to show up ...
waiting since 31 secs for 10.34.34.0/24 route to show up ...
waiting since 41 secs for 10.34.34.0/24 route to show up ...
10.34.34.0/24 via 10.12.12.12 dev eth0 proto 22 

kernel version reported by container r1: Linux r1 5.4.32-47.container #1 SMP Mon Apr 20 16:13:33 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux


checking routing table in network namespace for r1 ...
r1 has pid 25222
default via 10.12.12.1 dev eth0 
10.12.12.0/24 dev eth0 proto kernel scope link src 10.12.12.11 
Great. No routes learned in the container network namespace (only within the kata VM)

ip route table on r1:
default via 10.12.12.1 dev eth0 
10.12.12.0/24 dev eth0 proto kernel scope link src 10.12.12.11 
10.23.23.0/24 via 10.12.12.12 dev eth0 proto 22 
10.34.34.0/24 via 10.12.12.12 dev eth0 proto 22 
172.16.16.0/24 via 10.12.12.1 dev eth0 proto 22 

ip -6 route table on r1:
::1 dev lo proto kernel metric 256 pref medium
10:12:12::/64 dev eth0 proto kernel metric 256 pref medium
10:23:23::/64 via fe80::42:aff:fe0c:c0c dev eth0 proto 22 metric 1024 pref medium
10:34:34::/64 via fe80::42:aff:fe0c:c0c dev eth0 proto 22 metric 1024 pref medium
fe80::/64 dev eth0 proto kernel metric 256 pref medium
default via 10:12:12::1 dev eth0 metric 1024 pref medium

ping r4 eth0 interface from r1:
PING 10.34.34.14 (10.34.34.14) 56(84) bytes of data.
....
--- 10.34.34.14 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.135/0.306/0.594/0.205 ms, ipg/ewma 0.450/0.492 ms

ping6 r4 eth0 interface from r1:
PING 10:34:34::14(10:34:34::14) 56 data bytes
....
--- 10:34:34::14 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.151/0.281/0.537/0.181 ms, ipg/ewma 0.366/0.447 ms
```

To show the running containers:

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND                 CREATED             STATUS              PORTS                                                              NAMES
b7d2e65c68f1        crpd:20.1R1.11      "/sbin/runit-init.sh"   13 minutes ago      Up 13 minutes       22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp   r1
062f3d1c4eb7        crpd:20.1R1.11      "/sbin/runit-init.sh"   13 minutes ago      Up 13 minutes       22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp   r2
188c49064d46        crpd:20.1R1.11      "/sbin/runit-init.sh"   13 minutes ago      Up 13 minutes       22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp   r3
be1cdfebbd59        crpd:20.1R1.11      "/sbin/runit-init.sh"   13 minutes ago      Up 13 minutes       22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp   r4
```

and to see the qemu processes running them:

```
$ ps ax|grep kata-runtime
24942 ?        Sl     0:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/b7d2e65c68f15fbd30d32b57670cbfb2fdcc7695fe723b7e62bdebf4c9f7025c -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-kata-runtime
24975 ?        Sl     0:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/be1cdfebbd59e5eeb86bd2d3eb85b74b35a10ed1ec8db277f70cc43ce87102ef -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-kata-runtime
25060 ?        Sl     0:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/062f3d1c4eb7d2973b46f3c46d67067b7ee14da0dbdf724a068cb9793e521c70 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-kata-runtime
25073 ?        Sl     0:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/188c49064d46c34cac1ee57dafac1ec6ad8a1e943527753ceb89f28062c33cf4 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-kata-runtime
26404 pts/1    S+     0:00 grep --color=auto kata-runtime
```

and finally 4 qemu processes:

```
$ ps ax|grep qemu|grep -v qemu|wc -l
4
```
