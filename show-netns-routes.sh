#!/bin/bash
# Copyright (c) 2020, Juniper Networks, Inc.
# All rights reserved.

container=$1

if [ -z "$1" ]; then
  echo "$0 <container>"
  exit 1
fi

sudo mkdir -p /var/run/netns
fc1=$(docker-compose ps -q $container)
#echo "$container $fc1"
pid1=$(docker inspect -f "{{.State.Pid}}" $fc1)
if [ -z "$pid1" ]; then
    echo "Can't find pid for container $container"
    exit 1
fi

echo "$container has pid $pid1"

sudo ln -sf /proc/$pid1/ns/net /var/run/netns/$container

sudo ip netns exec $container ip route

#containerecho "setting ip addresses $ipv4 and $ipv6 on $int ..."
#sudo ip netns exec $container ip addr add $ipv4 dev $int
#sudo ip netns exec $container ip -6 addr add $ipv6 dev $int

#echo "enable interface $int"
#sudo ip netns exec $container ip link set up $int

