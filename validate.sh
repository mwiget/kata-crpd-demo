#!/bin/bash
# Copyright (c) 2020, Juniper Networks, Inc.
# All rights reserved.

echo ""
echo "kernel version: $(uname -a)"
echo "executed from $(pwd) on $(hostname)"
echo ""

SECONDS=0
while true; do
  docker exec -ti r1 ip route|grep 10.34.34.0 && break
  echo "waiting since $SECONDS secs for 10.34.34.0/24 route to show up ..."
  sleep 10
done

echo ""
echo -n "kernel version reported by container r1: "
docker exec -ti r1 uname -a
echo ""

echo ""
echo "checking routing table in network namespace for r1 ..."
./show-netns-routes.sh r1
if ./show-netns-routes.sh r1 | grep 10.34.34.0; then
  echo "found it. Not good. Likely not using kata runtime "
  exit 1
else
  echo "Great. No routes learned in the container network namespace (only within the kata VM)"
fi

echo ""
echo "ip route table on r1:"
docker exec -ti r1 ip route

echo ""
echo "ip -6 route table on r1:"
docker exec -ti r1 ip -6 route

echo ""
echo "ping r4 eth0 interface from r1:"
docker exec -ti r1 ping -f -c 3 10.34.34.14 || echo "FAILED"

echo ""
echo "ping6 r4 eth0 interface from r1:"
docker exec -ti r1 ping6 -f -c 3 10:34:34::14 || echo "FAILED"
